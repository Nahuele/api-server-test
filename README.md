## Instrucciones

Instalar [Node.js](https://nodejs.org/es/)

Crear una DB de ejemplo (como la del archivo en [assets](https://gitlab.com/Nahuele/api-server-test/-/blob/master/assets/convertcsv.sql))

Tener encendido el servicio de MySQL

`sudo service mysql start`

Clonar el Repositorio 

`git clone https://gitlab.com/Nahuele/api-server-test.git`

Eso va a crear una carpeta, entramos a la carpeta e instalamos los modulos:

`npm install`

Ejecutar el servidor con:

`npm run dev`

Visitar la url para acceder a los datos en formato JSON:

http://localhost:3000 (vista de bienvenida)

http://localhost:3000/clusters (accede a la DB completa y muestra el contenido como json)

http://localhost:3000/clusters/codigoPDB (accede a la DB de maximos y muestra info para ese codigo/cluster)


Para hacer las request (en este caso GET) se puede usar la URL, o programas como externos [PostMan](https://www.postman.com/use-cases/api-testing-automation/) o [Insomnia](https://insomnia.rest/) aunque esto es opcional.
