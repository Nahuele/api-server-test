import { Router} from 'express';
import {principalController} from '../controllers/index.controller';


const router = Router();

router.route('/').get(principalController	)

export default router;

