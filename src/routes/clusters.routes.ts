import {Router} from 'express';
import {getClusters, getCluster} from '../controllers/clusters.controller';

const router = Router();

router.route('/')
	.get(getClusters)

router.route('/:clusterID')
	.get(getCluster)

export default router;
