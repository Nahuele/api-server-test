import  express, {Application} from 'express';
import morgan from 'morgan';
import IndexRouter from './routes/index.routes';
import ClusterRoutes from './routes/clusters.routes';

let cors = require('cors')

export class App {
	private app: Application;

	constructor(private port? : number | string) {
		this.app = express();
		this.settings();
		this.middlewares();
		this.routes();
	}

	settings(){
		// aca toma un puerto como argumento, o se fija que exista variable de entorno, o le asigna el 3000 si lo demas no se cumple
		this.app.set('port', this.port || process.env.PORT || 3000)
		this.app.use(cors())
	}

	//midlewar de morgan
	middlewares() {
		this.app.use(morgan('dev'));
	}

	// rutas
	routes() {
		this.app.use(IndexRouter);
		this.app.use('/clusters', ClusterRoutes)
	}


	// async para escuchar el puerto
	async  listen() {
		await this.app.listen(this.app.get('port'));
		console.log('server on port 3000');
	}
}
