import {Request, Response} from 'express';
// llamando a la DB
import {connectMax, connectTotal} from '../database';

export async function getClusters(req: Request, res: Response): Promise<Response> {
	const consulta = await connectTotal();
	const clusters = await consulta.query('SELECT * from datos');
	return res.json(clusters[0]);
}

export async function getCluster(req: Request, res: Response): Promise<Response> {
	const clstrID = req.params.clusterID;
	const consulta = await connectMax();
	const clusters = await consulta.query('SELECT * FROM mini_max_datos WHERE Cluster_ID = ?', [clstrID]);
	return res.json(clusters[0]); // devuelve el cluster que coincide con el clstrID que le pasamos
}
