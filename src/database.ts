import { createPool } from 'mysql2/promise';

export async  function connectTotal() {
	const connection = await createPool({
		host: 'localhost',
		user: 'root',
		password: 'toor1',
		database: 'mini_test',
		connectionLimit: 10,
	});
	return connection;
}

export async  function connectMax() {
	const connection = await createPool({
		host: 'localhost',
		user: 'root',
		password: 'toor1',
		database: 'mini_max_test',
		connectionLimit: 10,
	});
	return connection;
}
